const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";

var output = { emails: [] };

jsonfile.readFile(inputFile, function (err, body) {
  if (err) {
    console.log(err.message);
    return;
  }
  console.log("Contents in input2.json", body);
  for (let i = 0; i < body.names.length; i++) {
    let reversed = body.names[i].split("").reverse().join("");

    reversed += randomstring.generate(5) + "@gmail.com";
    output.emails.push(reversed);
  }
  jsonfile.writeFile(outputFile, output, { spaces: 4 }, function (err) {
    console.log("Fake emails produced in output2.json");
  });
});
